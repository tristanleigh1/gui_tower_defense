CS 257 Software Design Final Project: Circle Defend'r

Group Members:
Jonah Tuchow, Tristan Leigh, and Sam Boswell

Running the Game:
Compile the .java files in the /src directory
Run Main from the command line, execute "java Main"

Summary:
In Circle Defend'r you play as squares who are under siege by enemy circles. Your
goal is to survive the onslaught by building square towers to defeat the circles
before they reach the end of the path. You click on the tower button to build your
towers, and they automatically defeat your foe.

Towers cost 50 gold each, fire blobs to destroy the circles.

Place them neighboring the path for increased results! Circle Defend'r has 7 waves of
circle enemies, ending with the Circle King trying to destroy your home.

Go forth and defeat the Circle enemy!